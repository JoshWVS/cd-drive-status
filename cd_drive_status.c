// Adapted from https://stackoverflow.com/a/56696933
#include <sys/ioctl.h>    // Provides ioctl()
#include <linux/cdrom.h>  // Provides struct and #defines
#include <fcntl.h>        // Ditto
#include <errno.h>        // Provides errno
#include <stdio.h>        // Provides printf(), fprintf()
#include <string.h>       // Provides strcmp

char *DEFAULT_PATH = "/dev/cdrom";

void help(char *argv[]) {
    printf("Usage: %s PATH_TO_DRIVE (defaults to %s)\n", argv[0], DEFAULT_PATH);
    printf("This program uses ioctl to check the status of the indicated CD drive.\n");
    printf("Exit value: on error, -1; otherwise, the return value of the ioctl call.\n");
    printf("The possible return values of the ioctl call are:\n");
    printf("0 CDS_NO_INFO (ioctl is not supported)\n");
    printf("1 CDS_NO_DISC\n");
    printf("2 CDS_TRAY_OPEN\n");
    printf("3 CDS_DRIVE_NOT_READY\n");
    printf("4 CDS_DISC_OK\n");
}

int main(int argc, char *argv[]) {
    char *path;
    if (argc == 1) {
        path = DEFAULT_PATH;
    } else if (argc == 2) {
        if (strcmp(argv[1], "--help") == 0 || strcmp(argv[1], "-h") == 0) {
            help(argv);
            return 0;
        } else {
            path = argv[1];
        }
    } else {
        printf("Error: only one (optional) argument is accepted, but you provided %d\n", argc - 1);
        help(argv);
        return -1;
    }
    int fd = open(path, O_RDONLY | O_NONBLOCK);
    if (errno != 0) {
        fprintf(stderr, "Error opening file: %u\n", errno);
        return -1;
    }
    int ret = ioctl(fd, CDROM_DRIVE_STATUS);
    return errno != 0 ? -1 : ret;
}
